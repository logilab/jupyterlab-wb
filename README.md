# Jupyterlab-wb

Whereby in jupyterlab

## Requirements

    python >=3.6
    jupyterlab ==3.*

## Install

pip install git+https://gitlab.com/logilab/jupyterlab-wb.git

## Configuration

You can add a file .jupyter/logilab-training.json to configure the rooms

#### Example

```json
{
  "rooms": [
    https://whereby.com/room1,
    https://whereby.com/room2
    ]
}
```
