import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin,
  ILayoutRestorer,
} from "@jupyterlab/application";
import { ICommandPalette, WidgetTracker } from "@jupyterlab/apputils";
import { ILauncher } from "@jupyterlab/launcher";
import { LabIcon } from "@jupyterlab/ui-components";
import { Panel } from "@lumino/widgets";

import { WherebyWidget } from "./widget";
import videoIconStr from "../style/video-solid.svg";

/**
 * Initialization data for the whereby extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: "jupyterlab-wb",
  autoStart: true,
  requires: [ICommandPalette],
  optional: [ILauncher, ILayoutRestorer],
  activate: (
    app: JupyterFrontEnd,
    palette: ICommandPalette,
    launcher?: ILauncher,
    restorer?: ILayoutRestorer
  ) => {
    console.log("whereby extension activate");
    const { commands } = app;
    let widget: Panel;
    const icon = new LabIcon({
      name: "launcher:wb-icon",
      svgstr: videoIconStr,
    });
    const tracker = new WidgetTracker<Panel>({ namespace: "jupyyterlab-wb" });

    const command = "wb:open";
    const content = new WherebyWidget();
    widget = new Panel();
    widget.id = `id-Whereby`;
    widget.addClass(`jp-Whereby-wrapper`);
    widget.title.caption = "Whereby";
    widget.title.closable = false;
    widget.title.icon = icon;
    widget.addWidget(content);
    app.shell.add(widget, "right");

    commands.addCommand(command, {
      caption: "Open whereby",
      label: "Whereby",
      icon: (args) => (args["isPalette"] ? null : icon),
      execute: () => {
        if (!widget || widget.isDisposed) {
          if (!tracker.has(widget)) {
            tracker.add(widget).catch(void 0);
          }
        }
        app.shell.activateById(widget.id);
      },
    });

    // Add the command to the palette.
    palette.addItem({ command, category: "Visio" });
    // Add the command to the launcher
    if (launcher) {
      launcher.add({
        command,
        category: "Apps",
        rank: 0,
      });
    }
    // If available, restore the position
    if (restorer) {
      restorer
        .restore(tracker, { command, name: () => `id-Whereby` })
        .catch(console.warn);
    }
  },
};

export default extension;
