import React from "react";

import { ReactWidget } from "@jupyterlab/apputils";
import { ContentsManager } from "@jupyterlab/services";

/**
 * A Lumino Widget that wraps a Whereby Component.
 */

const WherebyComponent = (): JSX.Element => {
  const [activeRoom, setActiveRoom] = React.useState("");
  const [rooms, setRooms] = React.useState([]);

  const getRooms = async (): Promise<string[]> => {
    let rooms;
    try {
      const contents = new ContentsManager();
      const modelConfig = await contents.get(".jupyter/logilab-training.json", {
        type: "file",
        content: true,
      });
      const content = JSON.parse(modelConfig.content);
      rooms = content.rooms;
    } catch {
      console.log("no configuration file, revert to default");
      rooms = ["https://logilab.whereby.com/openspace-quater"];
    }
    return rooms;
  };

  React.useEffect(() => {
    getRooms().then((rooms) => {
      setRooms(rooms);
      setActiveRoom(rooms[0]);
    });
  }, []);

  return (
    <>
      <section>
        {rooms.map((room, index) => (
          <button key={room} onClick={() => setActiveRoom(room)}>
            room {index + 1}
          </button>
        ))}
      </section>
      <iframe
        src={activeRoom}
        allow="camera; microphone; fullscreen; speaker; display-capture"
        className="wb-iframe"
      ></iframe>
    </>
  );
};

export class WherebyWidget extends ReactWidget {
  /**
   * Constructs a new WherebyWidget.
   */
  constructor() {
    super();
    this.addClass("jp-Whereby-Widget");
  }

  render(): JSX.Element {
    return <WherebyComponent />;
  }
}
